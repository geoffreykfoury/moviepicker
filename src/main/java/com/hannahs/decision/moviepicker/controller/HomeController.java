package com.hannahs.decision.moviepicker.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

  @GetMapping("/home")
  public String mainHome() {
    return "/home";
  }

  @GetMapping("/login")
  public String login() {
    return "/login";
  }

  @GetMapping("/")
  public String defaultPage() {
    return "/home";
  }

  @GetMapping("/user")
  public String user() {
    return "/user";
  }

  @GetMapping("/403")
  public String error403() {
    return "/error/403";
  }

  @PostMapping("/movie/{genre}/{type}")
  public String movie(@PathVariable String genre, @PathVariable String type) {
    //TODO how to get this to redirect to movie page
    return "/movie";
  }

}
