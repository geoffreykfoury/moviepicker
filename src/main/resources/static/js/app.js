$(document).ready(function () {


  $('#submit').on('click', function (e) {

    var genre = ""
    var type = ""

    $('.genre-input:checkbox:checked').each(function () {
      genre += this.id + "-"
    });
    $('.type-input:radio:checked').each(function () {
      type = this.id;
    });

    if (genre === "") {
      alert("Must select a genre.")
    } else {
      $.ajax({
        type: 'POST',
        url: contextRoot + "movie/" + genre.substring(0, genre.length - 1) + "/" + type,
        dataType: 'json',
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Accept", "application/json");
          xhr.setRequestHeader("Content-type", "application/json");
        }
      })
    }
  });
});
